#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "library.h"

void owner_area(user_t *u)
{int choice;
	do {
		printf("\n\n0. Sign Out\n1. Appoint Librarian\n2. Edit Profile\n3. Change Password\n4. Fees/Fine Report\n5. Book Availability\n6. Book Categories/Subjects\nEnter choice: ");
		scanf("%d", &choice);   
		switch(choice) {
			case 1: 
				appoint_librarian();
				break;
			case 2:
			    profile_edit(u);
				break;
			case 3:
			    change_pwd( );
				break;
			case 4:
			    fund_report();
				break;
			case 5:
			    bookcopy_checkavailable() ;
				break;
			case 6:
			    books_list();
				break;
		}
	}while (choice != 0);	
}

void fund_report()
{
	payment_t p;
	FILE *fp;
	fp= fopen(PAYMENT_DB,"rb");
	if(fp == NULL)
	{
		perror("file can not open");
		return;
	}
	while(fread(&p,sizeof(payment_t),1,fp)>0)
	{
		payment_display(&p);
	}
    fclose(fp);
}

void books_list()
{
	book_t b;
	bookcopy_t bc;
	FILE *fp,*np;
	fp= fopen(BOOK_DB,"rb");

	if(fp == NULL)
	{
		perror("file can not open");
		return;
	}
	np= fopen(BOOKCOPY_DB,"rb");
	if(fp == NULL)
	{
		perror("file can not open");
	}
	while(fread(&b,sizeof(book_t),1,fp)>0)
	{  
		book_display(&b);
		while(fread(&bc,sizeof(bookcopy_t),1,np)>0)
		{
			if(b.bookid == bc.bookid)
			bookcopy_display(&bc);
		}
	}	
	fclose(fp);
	fclose(np);
}






void appoint_librarian()
{ user_t u ;
  user_accept(&u);
  strcpy(u.role, ROLE_LIBRARIAN);
  user_add (&u);
}

void profile_edit(user_t *u)
{  FILE *fp;
   user_t u1;
   int found = 0;
   fp = fopen(USER_DB, "rb+");
   if(fp == NULL)
   {  perror("failed to open users file");
	  exit(1);
   }
   while(fread(&u1,sizeof(user_t),1,fp)>0)
   {
	   if(strcmp(u->email,u1.email)==0)
	   { found= 1;
	     break;
	    }
   }
   if(found) {
		long size = sizeof(user_t);
		user_t nu;
		user_accept(&nu);
		nu.id = u1.id;
		fseek(fp, -size, SEEK_CUR);
		fwrite(&nu, size, 1, fp);
		printf("user profile updated.\n");
	}
	else 
		printf("user not found.\n");
   fclose(fp);
  // return found;
}


void change_pwd( ) 
{   
	int id, found = 0;
	FILE *fp;
	user_t u;
	printf("enter id: ");
	scanf("%d", &id);
	fp = fopen(USER_DB, "rb+");
	if(fp == NULL) 
	{
		perror("cannot open books file");
		return;
	}
	// read books one by one and check if book with given id is found.
	while(fread(&u, sizeof(user_t), 1, fp) > 0) 
	{
		if( id == u.id ) 
		{  
			found = 1;
			break;
		}
	}
	if(found) {
		long size = sizeof(user_t);
		user_t n;
		//user_accept(&n);
		//nu.id = u2.id;
		printf("enter new password");
		scanf("%s",n.password);

		n.id = u.id;

		strcpy(n.name,u.name);
        strcpy(n.mobileno,u.mobileno);
		strcpy(n.role,u.role);
		strcpy(n.email,u.email);

		fseek(fp, -size, SEEK_CUR);
		fwrite(&n, size, 1, fp);
		printf("password updated.\n");
	}
	else 
		printf("user not found.\n");
	fclose(fp); 
}


void bookcopy_checkavailable()   //day4
{
	int book_id;
	FILE *fp;
	bookcopy_t bc;
	int count = 0 ;
	printf("enter the book id:");
	scanf("%d",&book_id);
	fp=fopen(BOOKCOPY_DB,"rb");
	if( fp==NULL)
	{
		perror("cannot open bookcopies file");
		return;
	}
	while(fread(&bc , sizeof(bookcopy_t),1,fp)>0)
	{
		if(bc.bookid == book_id && strcmp(bc.status,STATUS_AVAIL)==0)
		{
			bookcopy_display(&bc);
			count++;
		}
	}
	fclose(fp);
	printf("number of copies availables: %d \n",count);
}
