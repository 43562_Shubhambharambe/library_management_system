#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "library.h"

void librarian_area(user_t *u)
{ int choice,pay;
  char name[50];
  
  
  do 
  {   printf("\n\n0. Sign Out\n1. Add member\n2. Edit Profile\n3. Change Password\n4. Add Book\n5. Find Book\n6. Edit Book\n7. Check Availability\n8. Add Copy\n9. Change Rack\n10. Issue Copy\n11. Return Copy\n12. Take Payment\n13. Payment History\n14.user list \nEnter choice: ");
		scanf("%d", &choice);

        switch (choice)
        {   case 1: //day3
		        add_member();
				break;
			case 2:
			    profiles_edit(u);
				break;
			case 3:
				changepassword( );
				break;
			case 4:
			       add_book();
				break;
			case 5: //day3
			       printf("Enter book name: ");  
				   scanf("%s", name);
				   book_find_by_name(name);
				break;
			case 6: 
			       book_edit_by_id();
				break;
			case 7:
			       bookcopy_checkavail_details();
				break;
			case 8:
			       bookcopy_add();
				break;
			case 9:
			       change_rack();
				break;
			case 10:
			       bookcopy_issue( );
				break;
			case 11:
			       bookcopy_return( );
				break;
			case 12:
			       take_payment( );
				break;
			case 13:
			       printf("enter member id");
				   scanf("%d",&pay);
			       payment_history(pay);
				break;
			case 14:
			       users_list();
				break;
        }
    }while (choice != 0);		
}

void users_list()
{
	user_t u;
	FILE *fp;
	fp= fopen(USER_DB,"rb");

	if(fp == NULL)
	{
		perror("file can not open");
		return;
	}
	while(fread(&u,sizeof(user_t),1,fp)>0)
	{
		user_display(&u);
	}
	
	fclose(fp);
}





void change_rack()
{
    int id,found=0 ;
	FILE *fp;
	bookcopy_t bc;
	printf("enter book id: ");
	scanf("%d",&id);

	fp=fopen(BOOKCOPY_DB,"rb+");
	if(fp==NULL)
	{
		perror("bookcopy not found");
		exit(1);
	}
	while(fread(&bc,sizeof(bookcopy_t),1,fp)>0)
	{
		if(id==bc.id)
		{
			found = 1;
			break;
		}
	}
	if(found)
	{  
		long size = sizeof(bookcopy_t);
		bookcopy_t nbc;
		bookcopy_accept(&nbc);
		nbc.id=bc.id;
		fseek(fp,-size,SEEK_CUR);
		fwrite(&nbc,size,1,fp);
		printf("rack changed successfully \n");		
	}
	else
	
		printf("rack not changed");
	fclose(fp);	
}




void payment_history(int member_id)   //day4
{
	FILE *fp;
	payment_t p;
	fp=fopen(PAYMENT_DB,"rb");
	if (fp == NULL)
	{
		perror("cannot find payment invoice");
		return;
	}
    while(fread(&p ,sizeof(payment_t),1,fp)>0)
	{
		if(p.memberid == member_id)
		   payment_display(&p);
	}
	fclose(fp);
}





void take_payment()
{
	/*payment_t p;
	payment_accept(&p);        // working but following sir's code cndn so put it in comment
	payment_add(&p);
	payment_display(&p);
	*/
    FILE *fp;
	payment_t p;
	payment_accept(&p);
	p.id = get_next_payment_id();
	fp = fopen(PAYMENT_DB, "ab");
	if(fp == NULL) {
		perror("cannot open payment file");
		exit(1);
	}
	fwrite(&p, sizeof(payment_t), 1, fp);
	fclose(fp);
}

void add_member() {
	// input member details
	user_t u;
	user_accept(&u);
	// add librarian into the users file
	user_add(&u);
}
//still nt understnd perfectly
void add_book() {
	FILE *fp;
	// input book details
	book_t b;
	book_accept(&b);
	b.bookid = get_next_book_id();
	// add book into the books file
	// open books file.
	fp = fopen(BOOK_DB, "ab");
	if(fp == NULL) {
		perror("cannot open books file");
		exit(1);
	}
	// append book to the file.
	fwrite(&b, sizeof(book_t), 1, fp);
	printf("book added in file.\n");
	// close books file.
	fclose(fp);
}


void profiles_edit(user_t *u)
{  FILE *fp;
   user_t u1;
   int found = 0;
   fp = fopen(USER_DB, "rb+");
   if(fp == NULL)
   {  perror("failed to open users file");
      exit(1);
    //  return found;
   }
   while(fread(&u1,sizeof(user_t),1,fp)>0)
   {
	   if(strcmp(u->email,u1.email)==0)
	   { found= 1;
	     break;
	    }
   }
   if(found) {
		long size = sizeof(user_t);
		user_t nu;
		user_accept(&nu);
		nu.id = u1.id;
		fseek(fp, -size, SEEK_CUR);
		fwrite(&nu, size, 1, fp);
		printf("user profile updated.\n");
	}
	else 
		printf("user not found.\n");
   fclose(fp);
  // return found;
}

void changepassword( ) 
{   
	int id, found = 0;
	FILE *fp;
	user_t u;
	printf("enter id: ");
	scanf("%d", &id);
	fp = fopen(USER_DB, "rb+");
	if(fp == NULL) 
	{
		perror("cannot open books file");
		return;
	}
	// read books one by one and check if book with given id is found.
	while(fread(&u, sizeof(user_t), 1, fp) > 0) 
	{
		if( id == u.id ) 
		{  
			found = 1;
			break;
		}
	}
	if(found) {
		long size = sizeof(user_t);
		user_t n;
		//user_accept(&n);
		//nu.id = u2.id;
		printf("enter new password");
		scanf("%s",n.password);

		n.id = u.id;

		strcpy(n.name,u.name);
        strcpy(n.mobileno,u.mobileno);
		strcpy(n.role,u.role);
		strcpy(n.email,u.email);

		fseek(fp, -size, SEEK_CUR);
		fwrite(&n, size, 1, fp);
		printf("password updated.\n");
	}
	else 
		printf("user not found.\n");
	fclose(fp); 
}















void book_edit_by_id() {
	int id, found = 0;
	FILE *fp;
	book_t b;
	printf("enter book id: ");
	scanf("%d", &id);
	fp = fopen(BOOK_DB, "rb+");
	if(fp == NULL) {
		perror("cannot open books file");
		exit(1);
	}
	// read books one by one and check if book with given id is found.
	while(fread(&b, sizeof(book_t), 1, fp) > 0) {
		if(id == b.bookid) {
			found = 1;
			break;
		}
	}
	if(found) {
		long size = sizeof(book_t);
		book_t nb;
		book_accept(&nb);
		nb.bookid = b.bookid;
		fseek(fp, -size, SEEK_CUR);
		fwrite(&nb, size, 1, fp);
		printf("book updated.\n");
	}
	else 
		printf("Book not found.\n");
	fclose(fp);
}

void bookcopy_add() {
	FILE *fp;
	bookcopy_t b;
	bookcopy_accept(&b);
	b.id = get_next_bookcopy_id();
	fp = fopen(BOOKCOPY_DB, "ab");
	if(fp == NULL) {
		perror("cannot open book copies file");
		exit(1);
	}
	// append book to the file.
	fwrite(&b, sizeof(bookcopy_t), 1, fp);
	printf("book copy added in file.\n");
	fclose(fp);
}


void bookcopy_checkavail_details()   //day4
{
	int book_id;
	FILE *fp;
	bookcopy_t bc;
	int count = 0 ;
	printf("enter the book id:");
	scanf("%d",&book_id);
	fp=fopen(BOOKCOPY_DB,"rb");
	if( fp==NULL)
	{
		perror("cannot open bookcopies file");
		return;
	}
	while(fread(&bc , sizeof(bookcopy_t),1,fp)>0)
	{
		if(bc.bookid == book_id && strcmp(bc.status,STATUS_AVAIL)==0)
		{
			bookcopy_display(&bc);
			count++;
		}
	}
	fclose(fp);
	if(count==0)
	printf("no book copies are available\n ");
}

void bookcopy_issue()    //day4
{   // accept issuerecord details from user
    //TODO: if user is not paid, give error & return.
	// generate & assign new id for the issuerecord
    // open issuerecord file
    // append record into the file
	// close the file
	// mark the copy as issued
     issuerecord_t rec;
	 FILE *fp;
	 issuerecord_accept(&rec);
     if(!is_paid_member(rec.memberid))
	  {
		printf("member is not paid.\n");
		return;
	  }
     rec.id =get_next_issuerecord_id();
	 fp= fopen(ISSUERECORD_DB,"ab");
	 if(fp ==NULL)
	 {
		 perror("issue record file cannot be open");
		 exit(1);
	 }
     fwrite(&rec,sizeof(issuerecord_t),1,fp);
	 fclose(fp);
	 bookcopy_changestatus(rec.copyid, STATUS_ISSUED);
}

void bookcopy_changestatus(int bookcopy_id, char status[])  //day4
{
     bookcopy_t bc;
	 FILE *fp;
	 long size= sizeof(bookcopy_t);
	 fp=fopen(BOOKCOPY_DB,"rb+");
	 if (fp ==NULL)
	 {
		 perror("cannot open book copies file ");
		 return;
	 }
     while(fread(&bc, sizeof(bookcopy_t),1,fp)>0)
	 {
		 if(bookcopy_id ==bc.id)
		 {
			 strcpy(bc.status, status);
			 fseek(fp, -size, SEEK_CUR);
			 fwrite(&bc,sizeof(bookcopy_t),1,fp);
			 break;
		 }
	 }
	 fclose(fp);
}

void display_issued_bookcopies(int member_id)   //day4
{
	FILE *fp;
	issuerecord_t rec;
	fp=fopen(ISSUERECORD_DB,"rb");
	if (fp == NULL)
	{
		perror("cannot open issue record file");
		return;
	}
    while(fread(&rec ,sizeof(issuerecord_t),1,fp)>0)
	{
		if(rec.memberid == member_id && rec.return_date.day==0)
		   issuerecord_display(&rec);
	}
	fclose(fp);
}

void bookcopy_return()
{
    int member_id, record_id;
	FILE *fp;
	issuerecord_t rec;
	int diff_days, found = 0;
	long size = sizeof(issuerecord_t);
	printf("enter memberid: ");
	scanf("%d", &member_id);
	display_issued_bookcopies(member_id);
	printf("enter issue record id (to return): ");
	scanf("%d",&record_id);
	fp=fopen(ISSUERECORD_DB,"rb+");
	if(fp==NULL)
	{
		perror("cannot open issue record file");
		return;
	}
	while(fread(&rec, sizeof(issuerecord_t),1,fp)>0)
	{
		if(record_id==rec.id)
		{
			found = 1;
			rec.return_date= date_current();
			diff_days= date_cmp(rec.return_date,rec.return_duedate);
			if(diff_days>0)
			   {rec.fine_amount= diff_days* FINE_PER_DAY;
			    fine_payment_add(rec.memberid, rec.fine_amount);
				printf("fine amount Rs. %.2lf/- is applied.\n", rec.fine_amount);
			   }
			   break;
		}
	}

    if(found)
	{
		fseek(fp,-size, SEEK_CUR);
		fwrite(&rec,sizeof(issuerecord_t),1,fp);
		printf("issue record updated after returning book:\n");
		issuerecord_display(&rec);
		bookcopy_changestatus(rec.copyid, STATUS_AVAIL);
	}
	fclose(fp);
}

void fine_payment_add(int memberid, double fine_amount)
 {
	FILE *fp;
	payment_t p;
	p.id = get_next_payment_id();
	p.memberid = memberid;
	p.amount = fine_amount;
	strcpy(p.type, PAY_TYPE_FINE);
	p.tx_time = date_current();
	memset(&p.next_pay_duedate, 0, sizeof(date_t));
	fp = fopen(PAYMENT_DB, "ab");
	if(fp == NULL) 
	{
		perror("cannot open payment file");
		exit(1);
	}
	fwrite(&p, sizeof(payment_t), 1, fp);
	fclose(fp);
}

int is_paid_member(int memberid) 
{
	date_t now = date_current();
	FILE *fp;
	payment_t p;
	int paid = 0;
	fp = fopen(PAYMENT_DB, "rb");
	if(fp==NULL) {
		perror("cannot open payment file");
		return 0;
	}
	
	while(fread(&p, sizeof(payment_t), 1, fp) > 0) {
		if(p.memberid == memberid && p.next_pay_duedate.day != 0 && date_cmp(now, p.next_pay_duedate) < 0) {
			paid = 1;
			break;
		}
	}
	fclose(fp);
	return paid;
}



